package com.cvesoft.calculator.util;

import android.util.Log;

import com.cvesoft.calculator.math.link.LinkVar;

import java.text.DecimalFormat;

public class _ref {

    public static final int tempCalcMinPrec = 10;
    public static int tempPrec = 3;

    public static final char pi = 'π';
    public static final char sqrt = '√';
    public static final char integral = '∫';
    public static final char fraction = '#';

    public static final String emptyStringException = "empty";
    public static final String varUndefinedException = "varUndefined";

    public static String defaultDF = "#";

    static {
        if (tempPrec > 0) defaultDF += ".";
        for (int i = 0; i < tempPrec; i++)
            defaultDF += "#";
    }

    private static final boolean tracing = true;

    public static String tag(){
        return tag(1);
    }

    public static String tag(int internalCalls){
        StackTraceElement ste = Thread.currentThread().getStackTrace()[3 + internalCalls];
        String fullName = ste.getClassName();

        int temp = fullName.indexOf('$');
        if (temp != -1)
            fullName = fullName.substring(0,temp);

        if (!tracing)
            return fullName.substring(fullName.lastIndexOf('.') + 1);

        return fullName + "(" + fullName.substring(fullName.lastIndexOf('.') + 1) +
                ".java:" + ste.getLineNumber() + ")";
    }

    public static String format(double value) {
        String s = new DecimalFormat("#.##########").format(value);
        if (s.equals("-0"))
            return "0";
        else
            return s;
    }

    public static String format(double value, DecimalFormat df) {
        String s = df.format(value);
        if (s.equals("-0"))
            return "0";

        return s;
    }

    @Deprecated
    public static String tagOld(){
        StackTraceElement temp = Thread.currentThread().getStackTrace()[3];
        String fullName = temp.getClassName();

        if (!tracing)
            return fullName.substring(fullName.lastIndexOf('.') + 1);

        return fullName + "(" + fullName.substring(fullName.lastIndexOf('.') + 1) +
                ".java:" + temp.getLineNumber() + ")";
    }

    /** Returns true if the char is a number or letter, not an operation. */
    public static boolean isNumber(char j){
        if ((j >= '0' && j <= '9' || j == '.') || (j >= 'A' && j <= 'Z') || j == LinkVar.PI || j == 'e') {
            Log.v(_ref.tag(1), j + " is a number");
            return true;
        }
        Log.v(_ref.tag(1), j + " is not a number");
        return false;
    }


}