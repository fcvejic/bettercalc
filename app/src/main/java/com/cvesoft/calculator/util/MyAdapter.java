package com.cvesoft.calculator.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.cvesoft.calculator.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends ArrayAdapter<String> {

    int resource;
    ArrayList list;

    public MyAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.resource = resource;
        list = (ArrayList) objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        EditText view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = (EditText)inflater.inflate(resource, parent, false);
        } else {
            view = (EditText)convertView;
        }

        view.setText((CharSequence) list.get(position));
        if (position == list.size() - 1) {
            view.setCursorVisible(true);
            view.setLongClickable(true);
            view.setTextIsSelectable(true);
            view.setFocusable(true);
            view.requestFocus();
        } else {
            view.setCursorVisible(false);
            view.setLongClickable(false);
            view.setTextIsSelectable(false);
            view.setFocusable(false);
        }


        return view;
    }
}









