package com.cvesoft.calculator;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;

import com.cvesoft.calculator.util._ref;


public class VarsDialog extends Dialog {

    // FIXME remove tabs & input (start from scratch?) add arg bool "store" when creating
    // or seperate vars dialog and store dialog
    // TODO create list, select item -> get or set (depends on "store"); "+" btn for new var
    // EDIT input vars as lowercase but store as uppercase

    EditText tbVarName;
    EditText tbVarValue;
    Button btnAddVar;

    public VarsDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    public boolean setup(){

        getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        // <editor-fold desc="old_vars_dialog">
        /*tbVarName = (EditText) findViewById(R.id.tbVarName);
        tbVarValue = (EditText) findViewById(R.id.tbVarValue);
        btnAddVar = (Button) findViewById(R.id.btnAddVar);

        TextWatcher temp = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                btnAddVar.setEnabled(tbVarName.getText().length() == 1
                        && tbVarValue.getText().length() > 0);
                CharSequence cs = tbVarName.getText();
                if (cs.length() > 1) {
                    int sel = tbVarName.getSelectionStart();
                    tbVarName.setText(s.subSequence(0, 1));
                    if (sel > tbVarName.getText().length())
                        tbVarName.setSelection(tbVarName.getText().length());
                    else
                        tbVarName.setSelection(sel);
                }
                if (cs.length() == 1 && (cs.charAt(0) < 'A' || cs.charAt(0) > 'Z'))
                    tbVarName.setText("");
            }
        };

        tbVarName.addTextChangedListener(temp);
        tbVarValue.addTextChangedListener(temp);

        btnAddVar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                char name = tbVarName.getText().charAt(0);
                if (name < 'A' || name > 'Z')
                    throw new IllegalArgumentException(name + "cannot be var");

                String s = String.valueOf(tbVarValue.getText());
                Log.v(Ref.tag(), "Adding variable " + name + " = " + s);

                Table.set(name, s);
            }
        });

        try {
            TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
            tabHost.setup();

            TabHost.TabSpec tabSpec = tabHost.newTabSpec("define");
            tabSpec.setContent(R.id.tabDefine);
            tabSpec.setIndicator("Define");
            tabHost.addTab(tabSpec);

            tabSpec = tabHost.newTabSpec("list");
            tabSpec.setContent(R.id.tabList);
            tabSpec.setIndicator("List");
            tabHost.addTab(tabSpec);

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        */
        // </editor-fold>

        return true;

    }


    public void enter(char c) {
        try {
            tbVarName.setText(String.valueOf(c));
            tbVarValue.setText("0");
            tbVarValue.setSelection(0, 1);
        } catch (Exception e){
            Log.e(_ref.tag(), e.getMessage(), e);
        }
    }
}