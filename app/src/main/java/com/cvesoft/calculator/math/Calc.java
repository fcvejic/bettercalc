package com.cvesoft.calculator.math;

import android.util.Log;

import com.cvesoft.calculator.util._ref;
import com.cvesoft.calculator.math.link.*;

public class Calc {

    static final double dx = 1E-14; // FIXME switch to BigDecimal

    public static double differentiate(String function, double x){
        return differentiate(function, x, 'X');
    }

    /** Returns an approximation of the derivative of
     * the given function at the given point.
     * @param d independant variable (x by default) */
    // TEST derrivative
    public static double differentiate(String function, double x, char d){
        Chain f = new Chain(function);
        double a = f.getValueFor(d, x - dx);
        double b = f.getValueFor(d, x + dx);
        Log.e(_ref.tag(), "a = " + a + " ; b = " + b);
        return (b - a) / (2 * dx);
    }

    /** Returns an approximation of the integral of
     * the given function with the given limits.*/
    // TEST integral
    public static double integrate(String function, double a, double b, char d){
        double res = 0;
        Chain chain = new Chain(function);
        for (double i = a; i <= b; i += dx){
            Table.set(d, new LinkNum(i));
            res += chain.getValue();
        }

        return res;
    }

    public static double integrate(String function, double a, double b) throws Exception {
        return integrate(function,a,b,'X');
    }



}