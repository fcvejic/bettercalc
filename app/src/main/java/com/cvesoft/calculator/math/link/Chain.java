package com.cvesoft.calculator.math.link;

import android.util.Log;

import com.cvesoft.calculator.util._ref;

import java.text.DecimalFormat;

public class Chain extends Link {

    Link first;
    Link last;

    public static double calculate(String math){
        return new Chain(math).getValue();
    }
    /** Calculates the getValue of the math expression and returns the
     * result in the specified {@linkplain java.text.DecimalFormat}
     * @param df Default is 9 decimal points (#.##########)
     * @see #calculate(String)
     * @see _ref#format(double) */
    public static String calculate(String input, DecimalFormat df) {
        if (df != null)
            return _ref.format(calculate(input), df);
        return _ref.format(calculate(input));
    }

    public Chain(){
        first = last = null;
    }

    public Chain(String input){
        first = last = null;

        Log.v(_ref.tag(), "Converting String to Chain: " + input);

        if (input.length() == 0){
            Log.w(_ref.tag(), "Empty string.");
            return;
        }

        // used to temporarily memorize operations
        // so that they're added in the right order
        // according to their priority (see method named priority)
        LinkOp[] mem = new LinkOp[64]; int m = 0;

        // scans char by char of the input
        for (int i = 0; i < input.length(); i++){

            char j = input.charAt(i);
            Log.v(_ref.tag(), "Analizing " + j + "(" + i + ")");

            // <editor-fold desc="variables">
            if (j >= 'A' && j <= 'Z') {
                //checks for implicit multiplication:
                if (i > 0 && (_ref.isNumber(input.charAt(i - 1)) || input.charAt(i - 1) == ')')) {
                    mem[m++] = new LinkOp(LinkOp.IMP_MULT);
                    Log.i(_ref.tag(), "Implicit multiplication just occured at: " + i);
                }
                add(new LinkVar(j));
                Log.d(_ref.tag(), "Adding var to chain: " + j);
            }
            // </editor-fold>

            // <editor-fold desc="numbers">
            else if (j >= '0' && j <= '9' || j == '.') {
                //checks for implicit multiplication:
                if (i > 0 && (input.charAt(i-1) == _ref.pi || input.charAt(i-1) == 'e' ||
                        (input.charAt(i-1) >= 'A' && input.charAt(i-1) <= 'Z') || input.charAt(i - 1) == ')')) {
                    mem[m++] = new LinkOp(LinkOp.IMP_MULT);
                    Log.i(_ref.tag(), "Implicit multiplication just occured at: " + i);
                }
                // remembers where the number begins:
                int t = i;
                // finds where in the string the number ends:
                while (j >= '0' && j <= '9' || j == '.') {
                    if (++i >= input.length()) break;
                    j = input.charAt(i);
                }
                Log.d(_ref.tag(), "Adding number to chain: " + input.substring(t, i) + " (" + t + "," + i + ")");
                add(new LinkNum(input.substring(t, i--)));
                //Log.v(Ref.tag(), "Moving i to: " + input.charAt(i) + "(" + i + ")");
            }
            // </editor-fold>

            // <editor-fold desc="operations & stuff">
            else switch (j) {
                    case '(':
                        //checks for implicit multiplication:
                        if (i > 0 && (_ref.isNumber(input.charAt(i - 1)) || input.charAt(i - 1) == ')')) {
                            mem[m++] = new LinkOp(LinkOp.IMP_MULT);
                            Log.i(_ref.tag(), "Implicit multiplication just occured at: " + i);
                        }
                        // finds where the closing parenthesis is
                        int cp = i + 1;
                        for (int q = 1; cp < input.length(); cp++) {
                            char c = input.charAt(cp);
                            if (c == '(') q++;
                            if (c == ')') q--;
                            if (q == 0) break;
                        }
                        Log.d(_ref.tag(), "CP set to " + cp);

                        Log.i(_ref.tag(), "Recursion with: '" + input.substring(i + 1, cp) + "'");
                        Chain re = new Chain(input.substring(i + 1, cp));

                        add(re);
                        Log.i(_ref.tag(), "Recursion returned " + re.toString() + " Added to chain.");

                        i = cp;
                        if (i < input.length())
                            Log.v(_ref.tag(), "Moving i to: " + input.charAt(i) + "(" + i + ")");
                        break;

                    case '-':
                        // adds ! if it's (-a) and '-' if it-s (a-b)
                        if (i == 0) {
                            mem[m++] = new LinkOp(LinkOp.NEG);
                            Log.v(_ref.tag(), "Adding func '!' to mem at " + (m - 1));
                        } else switch (input.charAt(i - 1)) {
                            case '^':
                            case '*':
                            case '/':
                            case 'n':
                            case 'g':
                                mem[m++] = new LinkOp(LinkOp.NEG);
                                Log.v(_ref.tag(), "Adding func '!' to mem at " + (m - 1));
                                break;
                            case 's':
                                mem[m++] = new LinkOp((input.charAt(i - 2) == 'n') ? LinkOp.SUB : LinkOp.NEG);
                                Log.v(_ref.tag(), "Adding func " + mem[m - 1].toString() + " to mem at " + (m - 1));
                                break;
                            default:
                                mem[m++] = new LinkOp((byte) j);
                                Log.v(_ref.tag(), "Adding op '-' to mem at " + (m - 1));
                                break;
                        }
                        break;

                    case '+':
                    case '*':
                    case '/':
                    case '^':
                        mem[m++] = new LinkOp((byte) j);
                        Log.v(_ref.tag(), "Adding op '" + j + "' to mem at " + (m - 1));
                        break;
                    case 's':
                    case 'c':
                    case 't':
                        //checks for implicit multiplication:
                        if (i > 0 && (_ref.isNumber(input.charAt(i - 1)) || input.charAt(i - 1) == ')')) {
                            mem[m++] = new LinkOp(LinkOp.IMP_MULT);
                            Log.i(_ref.tag(), "Implicit multiplication just occured at: " + i);
                        }
                        mem[m++] = new LinkOp((byte) j);
                        Log.v(_ref.tag(), "Adding func at m = " + (m - 1) + "(" + j + ")");
                        i += 2;
                        Log.v(_ref.tag(), "Moving i to: " + input.charAt(i) + "(" + i + ")");
                        break;

                    case _ref.pi:
                        if (i > 0 && (_ref.isNumber(input.charAt(i - 1)) || input.charAt(i - 1) == ')')) {
                            mem[m++] = new LinkOp(LinkOp.IMP_MULT);
                            Log.i(_ref.tag(), "Implicit multiplication just occured at: " + i);
                        }
                        add(new LinkVar(j));
                        Log.d(_ref.tag(), "Adding " + j + " to chain.");
                        break;
                    case 'e':
                        if (i > 0 && (_ref.isNumber(input.charAt(i - 1)) || input.charAt(i - 1) == ')' ||
                                (i > 1 && input.charAt(i - 2) == 'n'))) {
                            mem[m++] = new LinkOp(LinkOp.IMP_MULT);
                            Log.i(_ref.tag(), "Implicit multiplication just occured at: " + i);
                        }
                        add(new LinkVar(j));
                        Log.d(_ref.tag(), "Adding e to chain.");
                        break;

                    case 'a':
                        //checks for implicit multiplication:
                        if (i > 0 && (_ref.isNumber(input.charAt(i - 1)) || input.charAt(i - 1) == ')')) {
                            mem[m++] = new LinkOp(LinkOp.IMP_MULT);
                            Log.i(_ref.tag(), "Implicit multiplication just occured at: " + i);
                        }
                        if (input.charAt(i + 1) == 'n') {
                            add(new LinkVar(LinkVar.ANS));
                            Log.d(_ref.tag(), "Adding 'ans' to chain.");
                            i += 2;
                            Log.v(_ref.tag(), "Moving i to: " + input.charAt(i) + "(" + i + ")");
                        } else switch (input.charAt(i + 3)) {
                            case 's':
                                mem[m++] = new LinkOp(LinkOp.aSIN);
                                Log.d(_ref.tag(), "Adding asin to chain.");
                                i += 5;
                                Log.v(_ref.tag(), "Moving i to: " + input.charAt(i) + "(" + i + ")");
                                break;
                            case 'c':
                                mem[m++] = new LinkOp(LinkOp.aCOS);
                                Log.d(_ref.tag(), "Adding acos to chain.");
                                i += 5;
                                Log.v(_ref.tag(), "Moving i to: " + input.charAt(i) + "(" + i + ")");
                                break;
                            case 't':
                                mem[m++] = new LinkOp(LinkOp.aTAN);
                                Log.d(_ref.tag(), "Adding atan to chain.");
                                i += 5;
                                Log.v(_ref.tag(), "Moving i to: " + input.charAt(i) + "(" + i + ")");
                                break;
                            default:
                                throw new IllegalArgumentException("Found an 'a' followed by " + input.charAt(i + 1));
                        }
                        break;
                    case _ref.sqrt:
                        if (i > 0 && (_ref.isNumber(input.charAt(i - 1)) || input.charAt(i - 1) == ')')) {
                            mem[m++] = new LinkOp(LinkOp.IMP_MULT);
                            Log.i(_ref.tag(), "Implicit multiplication just occured at: " + i);
                        }
                        mem[m++] = new LinkOp(LinkOp.SQRT);
                        break;
                    case 'l':
                        if (i > 0 && (_ref.isNumber(input.charAt(i - 1)) || input.charAt(i - 1) == ')')) {
                            mem[m++] = new LinkOp(LinkOp.IMP_MULT);
                            Log.i(_ref.tag(), "Implicit multiplication just occured at: " + i);
                        }
                        if (input.charAt(++i) == 'o') {
                            mem[m++] = new LinkOp(LinkOp.LOG);
                            i++;
                        } else
                            mem[m++] = new LinkOp(LinkOp.LN);
                        break;

                    default:
                        throw new IllegalArgumentException("Operation not recognized: " + j);
            }
            // </editor-fold>

            // <editor-fold desc="mem->chain">
            // adds operations from mem[] into chain when appropriate
            if (m >= 0){
                // when at the end of input string
                if (i >= input.length() - 1) {
                    Log.i(_ref.tag(), "Adding all ops to chain.");
                    while (m > 0) {
                        add(mem[--m]);
                        Log.d(_ref.tag(), "Adding op to chain: " + mem[m]);
                    }
                }
                // if the previous op in mem has priority over the latest one
                else if (!_ref.isNumber(j) && j != '(') {
                    while (m > 1 && LinkOp.priority(mem[m - 2], mem[m - 1])) {
                        Log.i(_ref.tag(), "Adding op to chain: " + mem[m-2] + " @" + (m-2));
                        add(mem[m - 2]);
                        mem[m - 2] = mem[--m];
                    }
                }
            }
            // </editor-fold>
        }

        Log.i(_ref.tag(), "Chain formed: " + toString());
    }

    public void add(Link link) {
        if (last == null)
            first = last = link;
        else {
            last.next = link;
            link.prev = last;
            last = link;
        }
    }

    public Link getFirst() {
        return first;
    }

    public Link getLast(){
        return last;
    }

    @Override
    public double getValue() {
        double temp = getLast().getValue();
        //Log.e(Ref.tag(), toString() + " = " + temp);
        return temp;
    }

    @Override
    public Link prev2() {
        return this;
    }

    @Override
    public String toString(){
        String res = "[";
        Link i = getFirst();
        while (i != null){
            res += i.toString() + " ";
            i = i.next();
        }
        return res.substring(0, res.length() - 1) + "]";
    }

}