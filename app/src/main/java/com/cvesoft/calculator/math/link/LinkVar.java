package com.cvesoft.calculator.math.link;

import com.cvesoft.calculator.math.Table;

public class LinkVar extends Link{

    public static final char PI = 'π';
    public static final char E = 'e';
    public static final char ANS = 0x01;

    private char var;

    public LinkVar(char c){
        var = c;
    }
    public LinkVar(String s){
        if (s.length() != 1)
            throw new IllegalArgumentException("String length has to be 1");
        var = s.charAt(0);
    }

    @Override
    public double getValue() {
        switch (var){
            case PI:
                return Math.PI;
            case E:
                return Math.E;
            default:
                return Table.get(var);
        }
    }

    @Override
    public Link prev2() {
        return this;
    }

    @Override
    public String toString(){
        if (var == ANS) return "ans";
        return String.valueOf(var);
    }

}
