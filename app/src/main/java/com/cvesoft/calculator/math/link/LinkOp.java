package com.cvesoft.calculator.math.link;

import android.util.Log;

import com.cvesoft.calculator.util._ref;

public class LinkOp extends Link{

    // 2 var ops
    public static final byte ADD = '+';
    public static final byte SUB = '-';
    public static final byte MULT = '*';
    public static final byte DIV = '/';
    public static final byte EXP = '^';

    public static final byte IMP_MULT = 0x21;
    public static final byte INT_DIV  = 0x22;
    public static final byte MOD      = 0x23;

    // 1 var ops
    public static final byte SIN = 's';
    public static final byte COS = 'c';
    public static final byte TAN = 't';

    public static final byte SQRT = 0x11;
    public static final byte aSIN = 0x12;
    public static final byte aCOS = 0x13;
    public static final byte aTAN = 0x14;
    public static final byte LOG  = 0x15;
    public static final byte LN   = 0x16;
    public static final byte NEG  = 0x17;

    /** Returns true if operation 'a' has priorety over 'b'.
     * <p>Note: it is assumed that 'a' precedes 'b' in the expression.</p>*/
    public static boolean priority(LinkOp a, LinkOp b){
        // special case:
        // x^sin5 => sin has priority
        if (a.op == EXP && (b.priority() == 2)) {
            Log.w(_ref.tag(1), "(exception) Prioritizing " + b + " over " + a);
            return false;
        }
        // special case:
        // xsin5 => sin has priority
        if (a.priority() == 3 && b.priority() == 2) {
            Log.w(_ref.tag(1), "(exception) Prioritizing " + b + " over " + a);
            return false;
        }
        if (a.priority() == 2 && b.priority() == 2){
            Log.w(_ref.tag(1), "(exception) Prioritizing " + b + " over " + a);
            return false;
        }

        if (a.priority() >= b.priority()) {
            Log.d(_ref.tag(1), "Prioritizing " + a + " over " + b);
            return true;
        }

        Log.d(_ref.tag(1), "Prioritizing '" + b + "' over '" + a + "'");
        return false;
    }

    /** Returns the priority of the operation represented by the char a.
     * Higher priority gives bigger number.
     * @throws java.lang.IllegalArgumentException if the operation hasn't been defined
     * @see #priority(LinkOp, LinkOp)  */
    public byte priority(){
        switch (op){
            case ADD:case SUB:
                return 0;
            case MULT:case DIV:
                return 1;
            case SIN:case COS:
            case TAN:case aSIN:
            case aCOS:case aTAN:
            case LOG:case LN:
            case SQRT:case NEG:
                return 2;
            // sin5x = sin(5x)
            case IMP_MULT:
                return 3;
            case EXP:
                return 5;
            default:
                throw new IllegalArgumentException(op + " not defined");
        }
    }

    private byte op;

    public LinkOp(byte op){
        this.op = op;
    }

    @Override
    public Link prev2(){
        switch (op){
            case ADD:case SUB:
            case MULT:case DIV:
            case EXP:case IMP_MULT:
            case INT_DIV:case MOD:
                Link temp = prev();
                while (temp != temp.prev2()) {
                    temp = temp.prev2();
                }
                return temp.prev();
            case SIN:case COS:
            case TAN:case SQRT:
            case aSIN:case aCOS:
            case aTAN:case LOG:
            case LN:case NEG:
                Link res = prev();
                while (res != res.prev2()) {
                    res = res.prev2();
                }
                return res;
            default:
                throw new UnknownError();
        }
    }

    @Override
    public double getValue() {
        Log.v(_ref.tag(), "Attempting: " + toString());
        double temp;
        switch (op){
            // 2 var ops
            case ADD:
                temp = prev2().getValue() + prev().getValue(); break;
            case SUB:
                temp = prev2().getValue() - prev().getValue(); break;
            case MULT:
            case IMP_MULT:
                temp = prev2().getValue() * prev().getValue(); break;
            case DIV:
                temp = prev2().getValue() / prev().getValue(); break;
            case EXP:
                temp = Math.pow(prev2().getValue(), prev().getValue()); break;
            case INT_DIV:
                temp = (int)(prev2().getValue() / prev().getValue()); break;
            case MOD:
                temp = prev2().getValue() % prev().getValue(); break;

            // 1 var ops
            case SQRT:
                temp = Math.sqrt(prev().getValue()); break;
            case SIN:
                temp = Math.sin(prev().getValue()); break;
            case COS:
                temp = Math.cos(prev().getValue()); break;
            case TAN:
                double a = prev().getValue();
                if (a % (2*Math.PI) == Math.PI / 2)
                    temp = Double.POSITIVE_INFINITY;
                if (a % (2*Math.PI) == 3 * Math.PI / 2)
                    temp = Double.NEGATIVE_INFINITY;
                temp = Math.tan(prev().getValue()); break;
            case aSIN:
                temp = Math.asin(prev().getValue()); break;
            case aCOS:
                temp = Math.acos(prev().getValue()); break;
            case aTAN:
                temp = Math.atan(prev().getValue()); break;
            case LOG:
                temp = Math.log10(prev().getValue()); break;
            case LN:
                temp = Math.log(prev().getValue()); break;
            case NEG:
                temp = -prev().getValue(); break;

            default:
                throw new UnknownError(op + " not defined");

        }
        Log.v(_ref.tag(), "Calculating " + prev2().toString() + toString() + prev().toString() + " = " + temp);
        return temp;
    }

    @Override
    public String toString() {
        switch (op){
            // 2 var ops
            case ADD:case SUB:
            case MULT:case DIV:
            case EXP:
                return String.valueOf((char)op);
            case IMP_MULT:
                return "im";
            case INT_DIV:
                return "div";
            case MOD:
                return "mod";

            // 1 var ops
            case SQRT:
                return String.valueOf(_ref.sqrt);
            case SIN:
                return "sin";
            case COS:
                return "cos";
            case TAN:
                return "tan";
            case aSIN:
                return "asin";
            case aCOS:
                return "acos";
            case aTAN:
                return "atan";
            case LOG:
                return "log";
            case LN:
                return "ln";
            case NEG:
                return "neg";

            default:
                throw new UnknownError(op + " not defined");

        }
    }

}
