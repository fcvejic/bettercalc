package com.cvesoft.calculator.math;

import android.util.Log;

import com.cvesoft.calculator.util._ref;
import com.cvesoft.calculator.math.link.*;

import java.util.ArrayList;

// used to store variables (constants and functions)
// for use in other math expressions
public class Table {

    // reserved spot for the ans variable
    public static final char ANS = 0x01;

    /** Variables are stored in instances of this class */
    private static class Var{
        char name;
        Link math;

        Var(char name, Link math){
            if ((name < 'A' || name > 'Z') && name != ANS)
                throw new IllegalArgumentException(name + " cannot be var");
            this.name = name;
            this.math = math;
        }
        char getName() { return name; }

        double getValue(){ return math.getValue(); }

        void set(Link math){ this.math = math; }
    }

    // storage
    private static ArrayList<Var> table = new ArrayList<>();

    /** Sets the specified var to the constant value. */
    public static void set(char name, double value) {
        set(name, new LinkNum(value));
    }

    /** Sets the specified var to the function represented by the string. */
    public static void set(char name, String s) {
        set(name, new Chain(s));
    }

    /** Sets the specified var to the functionor or number represented by the Link. */
    public static void set(char name, Link math) {
        Log.d(_ref.tag(), "Pushing " + name + "=" + math.toString() + " to table.");
        for (Var x : table)
            if (x.getName() == name){
                // if the var already exists, changes it
                x.set(math);
                return;
            }
        // if it doesnt, adds it
        table.add(new Var(name,math));
        Log.i(_ref.tag(), "Variable not found, adding it to table.");
    }

    /** Returns the numerical value of the var */
    public static double get(char name){
        Log.d(_ref.tag(), "Fetching value of " + name + " from table.");
        for (Var x : table)
            if (x.getName() == name){
                return x.getValue();
            }
        throw new IllegalArgumentException(name + _ref.varUndefinedException);
    }


}