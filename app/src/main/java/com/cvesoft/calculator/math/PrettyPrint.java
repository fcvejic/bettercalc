package com.cvesoft.calculator.math;

public class PrettyPrint {

    // http://htmledit.squarefree.com/
    // http://www.myphysicslab.com/web_math.html
    // TODO modify to use mathjax & asciimath (.org)

    public static String toHtml(String math){

        String res = "`" + math + "`";

        // TODO process and polish string, get ready for mathjax
        // or process Chain instead..?
        // !!! add parathesis for function (like sqrtpi^2)
        for (int i = 0; i < res.length(); i++){

            char j = res.charAt(i);
            if (j >= 'A' && j <= 'Z') {
                res = res.substring(0, i) + "<i>" + Character.toLowerCase(j) + "</i>" + res.substring(i + 1);
                i += 7;
            }




        }


        return res;


    }


}
