package com.cvesoft.calculator.math.link;

public class LinkNum extends Link {

    private double value;

    public LinkNum(String input){
        super();
        value = Double.valueOf(input);
    }

    public LinkNum(double number){
        super();
        value = number;
    }

    @Override
    public double getValue(){
        return value;
    }

    @Override
    public Link prev2() {
        return this;
    }

    @Override public String toString(){
        return String.valueOf(value);
    }

}
