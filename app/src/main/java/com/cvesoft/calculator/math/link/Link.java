package com.cvesoft.calculator.math.link;

import com.cvesoft.calculator.math.Table;

import java.text.DecimalFormat;

public abstract class Link {

    Link prev;
    Link next;

    public Link prev(){
        return prev;
    }
    public Link next(){
        return next;
    }

    public abstract double getValue();

    public String valueString(String df){
        return new DecimalFormat(df).format(getValue());
    }

    public String valueString(DecimalFormat df){
        return df.format(getValue());
    }

    public abstract Link prev2();

    public double getValueFor(char c, double d){
        Table.set(c,d);
        return getValue();
    }

}
