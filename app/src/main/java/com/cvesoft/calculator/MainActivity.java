package com.cvesoft.calculator;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.cvesoft.calculator.math.Table;
import com.cvesoft.calculator.math.link.Chain;
import com.cvesoft.calculator.util.MyAdapter;
import com.cvesoft.calculator.util._ref;

import java.util.ArrayList;


public class MainActivity extends Activity {

    // 1 fix the list
    // 2 switch to bigdecimal
    // 3 work on vars & store dialog OR
    // on calculus OR on settings (do . , thing)
    // EDIT enable rotation?
    // EDIT figure out how to do MathPrint
    // checkpoint: settings
    // EDIT maybe make a util class for prefs?

    // main multiline text box that
    // will contain both input and output
    ListView listView;
    MyAdapter adapter;
    ArrayList<String> history = new ArrayList<>();

    // toggles what buttons do
    ToggleButton btn2nd;

    VarsDialog varsDialog;
    ClipboardManager cbm;
    SharedPreferences prefs;

    // <editor-fold desc="tb_onclick_&long">
    public View.OnClickListener tbOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (getTB() == v) return;
            EditText temp = (EditText) v;
            String s = String.valueOf(temp.getText());
            if (s.indexOf('=') == -1) {
                Log.w(_ref.tag(), "This happened for some stupid reason.");
                return;
            }
            cbm.setPrimaryClip(ClipData.newPlainText("hist-exp", s.substring(0, s.indexOf('=') - 1)));
            Toast.makeText(MainActivity.this, "Copied expression.", Toast.LENGTH_SHORT).show();
        }
    };
    public View.OnLongClickListener tbOnLongClick = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            if (getTB() == v) return false;
            EditText temp = (EditText) v;
            String s = String.valueOf(temp.getText());
            if (s.indexOf('=') == -1) {
                Log.w(_ref.tag(), "This happened for some stupid reason. (long)");
                return false;
            }
            cbm.setPrimaryClip(ClipData.newPlainText("hist-ans", s.substring(s.indexOf('=') + 2)));
            Toast.makeText(MainActivity.this, "Copied answer.", Toast.LENGTH_SHORT).show();
            return true;
        }
    };
    //</editor-fold>

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        history.add("");
        listView = (ListView) findViewById(R.id.list);
        adapter = new MyAdapter(this, R.layout.simple_list_edit, history);
        listView.setAdapter(adapter);
        listView.post(listPost);

        btn2nd = (ToggleButton) findViewById(R.id.btn2nd);
        cbm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    }

    // <editor-fold desc="getTB">
    private EditText getTB(){
        return getTB(listView.getChildCount() - 1);
    }

    private EditText getTB(int i) {
        if (i >= listView.getChildCount()){
            Log.e(_ref.tag(), "Fix this: " + _ref.tag(1));
            throw new IndexOutOfBoundsException("i = " + i);
        }

        return (EditText) listView.getChildAt(i);
    }
    // </editor-fold>

    /** Types in the main textbox based on which button was pressed.
     * @see com.cvesoft.calculator.MainActivity#type(String)  */
    public void type(View button){
        String vib = prefs.getString("vibrate", "0"); // TODO prefs!!!
        if (vib.equals("0")) button.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
        if (vib.equals("1")) button.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP,
                HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);
        getTB().requestFocus();

        // <editor-fold desc="type_switch_case">
        switch (button.getId()) {
            case R.id.btn0:
                type("0");
                break;
            case R.id.btn1:
                type("1");
                break;
            case R.id.btn2:
                type("2");
                break;
            case R.id.btn3:
                type("3");
                break;
            case R.id.btn4:
                type("4");
                break;
            case R.id.btn5:
                type("5");
                break;
            case R.id.btn6:
                type("6");
                break;
            case R.id.btn7:
                type("7");
                break;
            case R.id.btn8:
                type("8");
                break;
            case R.id.btn9:
                type("9");
                break;
            case R.id.btnDec:
                type(".");
                break;
            case R.id.btnPi:
                type("π");
                break;
            case R.id.btnE:
                type("e");
                break;
            case R.id.btnAdd:
                type("+");
                break;
            case R.id.btnSub:
                type("-");
                break;
            case R.id.btnMul:
                type("*");
                break;
            case R.id.btnDiv:
                type("/");
                break;
            case R.id.btnAns:
                type("ans");
                break;
            case R.id.btnSin:
                if (btn2nd.isChecked())
                    type("arcsin");
                else
                    type("sin");
                break;
            case R.id.btnCos:
                if (btn2nd.isChecked()) {
                    type("arccos");
                    btn2nd.setChecked(false);
                } else
                    type("cos");
                break;
            case R.id.btnTan:
                if (btn2nd.isChecked())
                    type("arctan");
                else
                    type("tan");
                break;
            case R.id.btnLog:
                type("log");
                break;
            case R.id.btnLn:
                type("ln");
                break;
            case R.id.btnOpen:
                type("(");
                break;
            case R.id.btnClose:
                type(")");
                break;
            case R.id.btnX:
                type("X");
                break;
            case R.id.btnSqr:
                if (btn2nd.isChecked())
                    type(_ref.sqrt);
                else
                    type("^2");
                break;
            case R.id.btnExp:
                type("^");
                break;
            case R.id.btnDel:
                delete();
                break;
            case R.id.btnClear:
                if (getTB().getText().length() > 0)
                    getTB().setText("");
                else {
                    while (history.size() > 1)
                        history.remove(0);
                    adapter.notifyDataSetChanged();
                    listView.post(listPost);
                }
                break;
        }
        // </editor-fold>

        if (btn2nd.isChecked()) {
            btn2nd.setChecked(false);
            toggle2nd(null);
        }
    }

    private void type(char c) {
        type(String.valueOf(c));
    }

    /** Types the input string into the main text box. */
    private void type(String input){
        Log.v(_ref.tag(), "Typing " + input);
        int debug;
        String s = String.valueOf(getTB().getText());
        if (!s.equals("")) {
            String si = s.substring(0, getTB().getSelectionStart());
            int j = getTB().getSelectionEnd();
            String sj = "";
            if (j < s.length())
                sj = s.substring(j);

            getTB().setText(si + input + sj);
            getTB().setSelection(debug = si.length() + input.length());

        } else
            //If textbox is empty and operation is pressed, then assume operation is to be done on ans
            if(listView.getChildCount() > 1 && (input.equals("+") || input.equals("-") || input.equals("*") || input.equals("/"))) {
                getTB().setText("ans" + input);
                getTB().setSelection(debug = 4);
            } else {
                getTB().setText(input);
                getTB().setSelection(debug = input.length());
        }
        Log.v(_ref.tag(), "Moved cursor: " + debug);
    }

    /** Deletes whatever precedes the cursor in the main text box. */
    private void delete(){
        String s = String.valueOf(getTB().getText());
        int i = getTB().getSelectionStart();
        int j = getTB().getSelectionEnd();

        if (i < s.lastIndexOf("\n")){
            Log.e(_ref.tag(), "not implemented yet");
            return;
        }

        if (i != j){
            getTB().setText(s.substring(0, i) + s.substring(j));
            Log.v(_ref.tag(), "Deleted: " + s.substring(i,j));
            getTB().setSelection(i);
            return;
        }

        if (i == 0) {
            Log.d(_ref.tag(), "Cursor at 0; nothing to delete");
            return;
        }

        getTB().setText(s.substring(0, i -= getPreviousLength()) + s.substring(j));
        Log.v(_ref.tag(), "Deleted: " + s.substring(i,j));
        getTB().setSelection(i);


    }

    /** Moves the main text box's cursor based on which button was pressed. */
    public void move(View button){
        String vib = prefs.getString("vibrate", "0");
        if (vib.equals("0")) button.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
        if (vib.equals("1")) button.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP,
                HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);
        getTB().requestFocus();

        String s = String.valueOf(getTB().getText());
        if (!s.equals("")) {
            int j = getTB().getSelectionEnd();
            switch (button.getId()) {
                case R.id.btnLeft:
                    getTB().setSelection(j - getPreviousLength());
                    break;
                case R.id.btnRight:
                    getTB().setSelection(j + getNextLength());
                    break;
            }
            if (j != getTB().getSelectionEnd())
                Log.d(_ref.tag(), "Moved cursor: " + getTB().getSelectionEnd());
        }

    }

    /** Called only by btn2nd. Toggles the text
     * of certain buttons based on what they'll do. */
    public void toggle2nd(View view){
        String vib = prefs.getString("vibrate", "0");
        if (vib.equals("0")) view.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
        if (vib.equals("1")) view.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP,
                HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);

        Log.v(_ref.tag(), "Toggled button '2nd' to " + btn2nd.isChecked());
        if (view == null)
            Log.w(_ref.tag(1), "Method called with null view.");

        Button i;
        if (btn2nd.isChecked()){
            i = (Button)findViewById(R.id.btnSqr);
            i.setText(R.string.btnSqrt);
            i = (Button)findViewById(R.id.btnSin);
            i.setText(R.string.btnArcSin);
            i = (Button)findViewById(R.id.btnCos);
            i.setText(R.string.btnArcCos);
            i = (Button)findViewById(R.id.btnTan);
            i.setText(R.string.btnArcTan);
            i = (Button)findViewById(R.id.btnEnter);
            i.setText(R.string.btnExit);
            i = (Button)findViewById(R.id.btnVars);
            i.setText(R.string.btnStore);

            btn2nd.setBackgroundColor(0xE64EB4A9);
            // style EDIT change to drawable

        } else {
            i = (Button)findViewById(R.id.btnSqr);
            i.setText(R.string.btnSqr);
            i = (Button)findViewById(R.id.btnSin);
            i.setText(R.string.btnSin);
            i = (Button)findViewById(R.id.btnCos);
            i.setText(R.string.btnCos);
            i = (Button)findViewById(R.id.btnTan);
            i.setText(R.string.btnTan);
            i = (Button)findViewById(R.id.btnEnter);
            i.setText(R.string.btnEnter);
            i = (Button)findViewById(R.id.btnVars);
            i.setText(R.string.btnVars);
            // TODO change other buttons

            btn2nd.setBackgroundColor(0x00FFFFFF);
        }
    }

    /** Opens a varsDialog where you can define
     * and getType variables and functions. */
    public void btnVars(View view){
        String vib = prefs.getString("vibrate", "0");
        if (vib.equals("0")) view.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
        if (vib.equals("1")) view.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP,
                HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);

        varsDialog = new VarsDialog(this);
        varsDialog.setContentView(R.layout.dialog_vars);
        varsDialog.show();

        varsDialog.setup();
        // FIXME if btn2nd & string not empty then STORE

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /** Calculates the entered math expression.
     * @see Chain#calculate(String) */
    public void enter(View view) {
        String vib = prefs.getString("vibrate", "0");
        if (vib.equals("0")) view.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
        if (vib.equals("1")) view.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP,
                HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);

        if (btn2nd.isChecked()){
            Log.d(_ref.tag(), "Exiting");
            finish();
            return;
        }

        try {
            String s = String.valueOf(getTB().getText());

            if (s.isEmpty()) return;
            Log.v(_ref.tag(), "Attempting calculation");
            double value = Chain.calculate(s);
            Log.i(_ref.tag(), "Calculation successful, result = " + value);

            history.set(history.size() - 1, s + " = " + _ref.format(value));
            Table.set(Table.ANS, value);
            history.add("");

            adapter.notifyDataSetChanged();
            listView.post(listPost);

        } catch (Exception e) {
            if (e.getMessage().contains(_ref.varUndefinedException)){
                btnVars(view);
                varsDialog.enter(e.getMessage().charAt(0));
                return;
            }


            Log.e(_ref.tag(), "Calculation failed", e);
            new AlertDialog.Builder(this).setTitle("I pooped up somewhere!")
                    .setMessage(e.getMessage()).setNeutralButton("OK", null).show();
        }
    }

    // <editor-fold desc="utils">
    /** Returns the length of whatever precedes
     * the cursor in the main text box. */
    int getPreviousLength(){
        String s = String.valueOf(getTB().getText());
        int i = getTB().getSelectionStart();

        if (i == s.lastIndexOf("\n") + 1)
            return 0;

        switch (s.charAt(--i)){
            case 'n':case 'g':case 's':
                if (i - 1 >= 0 && s.charAt(i - 1) == 'l')
                    // ln
                    return 2;

                if (i - 3 >= 0 && s.charAt(i - 3) == 'a')
                    // arc trig
                    return 4;

                // trig & ans
                return 3;

            default:
                return 1;
        }

    }

    /***/
    int getNextLength(){
        String s = String.valueOf(getTB().getText());
        int i = getTB().getSelectionStart();

        if (i == s.length())
            return 0;

        switch (s.charAt(i)) {
            case 'a':
                if (s.charAt(i + 1) == 'n')
                    // ans
                    return 3;
                // arc trig
                return 4;

            case 'l':
                if (s.charAt(i + 1) == 'n')
                    // ln
                    return 2;
                // log
                return 3;

            case 'c':case 's':case 't':
                // trig
                return 3;

            default:
                return 1;
        }

    }
    // </editor-fold>

    // <editor-fold desc="Runnable listPost">
    private Runnable listPost = new Runnable() {
        @Override
        public void run() {
            for (int i = 0; i < listView.getChildCount(); i++) {
                getTB(i).setOnClickListener(tbOnClick);
                getTB(i).setOnLongClickListener(tbOnLongClick);
            }
        }
    };
    // </editor-fold>

    // <editor-fold desc="Tests">
    public void test1(View view) {
        Log.i(_ref.tag(), "Performing test 1");

        for (int i = 0; i < listView.getChildCount(); i++){
            getTB(i).setText(Html.fromHtml("<i>test</i> - this is " + i));
        }

    }
    public void test2(View view) {
        //noop
    }
    // </editor-fold>



}