package com.cvesoft.calculator;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

import com.cvesoft.calculator.R;

import java.util.List;

public class PrefsActivity extends PreferenceActivity{

    @Override
    public void onBuildHeaders(List<Header> target){
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    public static class prefs_basic extends PreferenceFragment{
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.prefs_basic);
        }
    }

}