package com.cvesoft.calculator;

import static junit.framework.Assert.*;

import com.cvesoft.calculator.math.Calc;
import com.cvesoft.calculator.math.Table;
import com.cvesoft.calculator.math.link.Chain;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.text.DecimalFormat;
//import android.support.test.runner.AndroidJUnit4;

@RunWith(JUnit4.class)
public class TestMath {
    DecimalFormat testingFormat = new DecimalFormat("#.###");

    @Test
    public void etf2002(){
        // fixed, should work
        String in = "3(.5/1.25+7/5/(1+4/7)-3/11)/((1.5+1/4)/(18+1/3))";
        assertEquals("32", Chain.calculate(in, testingFormat));
    }

    @Test
    public void tehf1990j(){
        String in = "(2/5+3/7*12/5)^-2";
        assertEquals("0.49", Chain.calculate(in, testingFormat));
    }

    @Test
    public void tehf1990s(){
        String in = "(4/9/(3/4)+(3/2)^-4)^-0.5";
        assertEquals("1.125", Chain.calculate(in, testingFormat));
    }

    @Test
    public void anse(){
        Table.set(Table.ANS, 5);
        String in = "anse";
        assertEquals("13.591", Chain.calculate(in, testingFormat));
    }

    @Test
    public void ansneg(){
        Table.set(Table.ANS, 5);
        String in = "ans-3";
        assertEquals("2", Chain.calculate(in, testingFormat));
    }

    //FIXME @Test
    public void sd1(){
        String in = "X^3+2X^2+1";
        assertEquals(20d, Calc.differentiate(in, 2d));
    }


}