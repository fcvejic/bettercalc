# README #

### Project summary ###

This is my first Android application, a practice problem if you will. It is a calculator that only calculates after you put the whole expression in, and will eventually use math print, unlike most calculators available on Google Play. Any help would be appreciated, as I am completely new to Android development.

### Set up ###

Standard Android Studio installation is what I use, but I'm sure Eclipse with ADT would work just as well.
The only setting that isn't default is the TODO tags (case-sensitive):

* FIXME high priority, marks something that needs to be fixed
* TODO regular priority, marks something that needs to be done
* EDIT low priority, marks something that I would like changed, but isn't necessary
* TEST low priority, marks something that I am not sure will work, needs to be tested yet

### Contribution guidelines ###

As I mentioned above, any help would be appreciated. I am new to android and fairly new to Java. If you see code that could be better, please do let me know.
If you find a bug while testing:

* the Math, please include:
    1. what you typed in
    2. the result it gave you
    3. what you believe the result should be
    4. (optional) why you think it's wrong
* the UI, please try to be as descriptive as possible

Feel free to contact me at <fcvejic@gmail.com>.